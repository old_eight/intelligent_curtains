/**
  ******************************************************************************
  * @file    MSD_test.c
  * @author  fire
  * @version V1.0
  * @date    2013-xx-xx
  * @brief   MSD测试代码
  ******************************************************************************
  * @attention
  *
  * 实验平台:秉火STM32 F103-指南者 开发板  
  * 论坛    :http://www.firebbs.cn
  * 淘宝    :http://firestm32.taobao.com
  *
  ******************************************************************************
  */ 

#include "MSD_test.h"  
#include <math.h>
#include <stdlib.h>
#include "usart.h"
#include "led.h"
#include "MicroStepDriver.h" 

int ydzt = 1;

void ShowData(int position, int acceleration, int deceleration, int speed, int steps);
void Delay(__IO u32 nCount);


/*! \brief 打印电机参数
 *  \param acceleration 加速度
 *  \param deceleration 减速度
 *  \param speed        最大速度
 *  \param steps        移动步数
 */
void ShowData(int position, int acceleration, int deceleration, int speed, int steps)
{
  UsartPrintf(USART_DEBUG, "\n\r加速度:%.2frad/s^2",1.0*acceleration/100);
  UsartPrintf(USART_DEBUG, "  减速度:%.2frad/s^2",1.0*deceleration/100);
  UsartPrintf(USART_DEBUG, "  最大速度:%.2frad/s(%.2frpm)",1.0*speed/100,9.55*speed/100);
  UsartPrintf(USART_DEBUG, "  移动步数:%d",steps);
  UsartPrintf(USART_DEBUG, "\n\r电机当前位置: %d\r\n",position);
}

void MotorRun(void)
{
	    static char showflag =1;
    //默认移动步数
    static int steps = 50000;
    //默认加速度
    static int acceleration = 2000;
    //默认减速度
    static int deceleration = 2000;
    //默认最大速度
    static int speed = 3000;
	
		uint8_t key_a = 0 ;
    
	  if(showflag)
    {
      showflag = 0;
      ShowData(stepPosition, acceleration, deceleration, speed, steps);
    }
		if(ydzt){
			MSD_Move(steps, acceleration, deceleration, speed);
		}
		
		if(status.running == TRUE)
		{
			if(status.out_ena == TRUE)
					UsartPrintf(USART_DEBUG, "\n\r电机正在运行...");
			while(status.running == TRUE)
			{
				
				key_a = Key_Scan(GPIOA,GPIO_Pin_0);
				if(key_a){
						MSD_ENA(ENABLE);
					  
					  ydzt = 0;
					  status.running = FALSE;
					  
				break;
				}
				
				if(status.out_ena != TRUE)
				break;
			};
			if(status.out_ena == TRUE)
			{
				UsartPrintf(USART_DEBUG, "OK\n\r");
				ShowData(stepPosition, acceleration, deceleration, speed, steps);  
			}

		}
	
}

void MotorFz(void)
{
	    static char showflag =1;
    //默认移动步数
    static int steps = -50000;
    //默认加速度
    static int acceleration = 2000;
    //默认减速度
    static int deceleration = 2000;
    //默认最大速度
    static int speed = 3000;
		
	  uint8_t key_a = 0 ;

    
	  if(showflag)
    {
      showflag = 0;
      ShowData(stepPosition, acceleration, deceleration, speed, steps);
    }
		
		if(ydzt){
				MSD_Move(steps, acceleration, deceleration, speed);
		}
		
		
		if(status.running == TRUE)
		{
				
			if(status.out_ena == TRUE)
					UsartPrintf(USART_DEBUG, "\n\r电机正在运行...");
			while(status.running == TRUE)
			{
				
				key_a = Key_Scan(GPIOA,GPIO_Pin_0);
				if(key_a){
						MSD_ENA(ENABLE);

						ydzt = 0;
						status.running = FALSE;

				break;
				}
			
				
				if(status.out_ena != TRUE)
				break;
			};
			
			if(status.out_ena == TRUE)
			{
				UsartPrintf(USART_DEBUG, "OK\n\r");
				ShowData(stepPosition, acceleration, deceleration, speed, steps);  
			}
				UsartPrintf(USART_DEBUG, "MSD_test_END\n\r");
		}
	
}


/*********************************************END OF FILE**********************/
