/**
  ******************************************************************************
  * @file    bsp_irda.c
  * @author  fire
  * @version V1.0
  * @date    2013-xx-xx
  * @brief   红外遥控器接口
  ******************************************************************************
  * @attention
  *
  * 实验平台:秉火 F103-指南者 STM32 开发板 
  * 论坛    :http://www.firebbs.cn
  * 淘宝    :https://fire-stm32.taobao.com
  *
  ******************************************************************************
  */
#include "stm32f10x_exti.h"
#include "bsp_irda.h" 
#include "delay.h"
#include "led.h"
#include "usart.h"
#include "MicroStepDriver.h"  
#include "MSD_test.h"  

uint32_t frame_data=0;    /* 一帧数据缓存 */
uint8_t  frame_cnt=0;
uint8_t  frame_flag=0;    /* 一帧数据接收完成标志 */

uint8_t isr_cnt;  /* 用于计算进了多少次中断 */
extern int ydzt;


 /**
  * @brief  配置嵌套向量中断控制器NVIC
  * @param  无
  * @retval 无
  */
static void NVIC_Configuration(void)
{
  NVIC_InitTypeDef NVIC_InitStructure;
  
  /* Configure one bit for preemption priority */
  //NVIC_PriorityGroupConfig(NVIC_PriorityGroup_1);
  
  /* 配置P[A|B|C|D|E]11为中断源 */
  NVIC_InitStructure.NVIC_IRQChannel = IRDA_EXTI_IRQN;
  NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 1;
  NVIC_InitStructure.NVIC_IRQChannelSubPriority = 4;
  NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
  NVIC_Init(&NVIC_InitStructure);
}

 
/* 初始化红外接收头1838用到的IO */
void IrDa_Init(void)
{
	GPIO_InitTypeDef GPIO_InitStructure; 
	EXTI_InitTypeDef EXTI_InitStructure;

	/* config the extiline clock and AFIO clock */
	RCC_APB2PeriphClockCmd(IRDA_GPIO_CLK | RCC_APB2Periph_AFIO,ENABLE);
												
	/* config the NVIC */
	NVIC_Configuration();

	/* EXTI line gpio config */	
  GPIO_InitStructure.GPIO_Pin = IRDA_GPIO_PIN;       
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;	 
  GPIO_Init(IRDA_GPIO_PORT, &GPIO_InitStructure);

	/* EXTI line mode config */
  GPIO_EXTILineConfig(IRDA_GPIO_PORT_SOURCE, IRDA_GPIO_PIN_SOURCE); 
  EXTI_InitStructure.EXTI_Line = IRDA_EXTI_LINE;
  EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt;
  EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Falling; //下降沿中断
  EXTI_InitStructure.EXTI_LineCmd = ENABLE;
  EXTI_Init(&EXTI_InitStructure); 
}

/* 获取高电平的时间 */
uint8_t Get_Pulse_Time(void)
{
  uint8_t time = 0;
  while( IrDa_DATA_IN() )
  {
    time ++;
    DelayUs(20);     // 延时 20us
    if(time == 250)
      return time;   // 超时溢出   
  }
  return time;
}

/*
 * 帧数据有4个字节，第一个字节是遥控的ID，第二个字节是第一个字节的反码
 * 第三个数据是遥控的真正的键值，第四个字节是第三个字节的反码
 */
uint8_t IrDa_Process(void)
{
  uint8_t first_byte, sec_byte, tir_byte, fou_byte;  
  
  first_byte = frame_data >> 24;
  sec_byte = (frame_data>>16) & 0xff;
  tir_byte = frame_data >> 8;
  fou_byte = frame_data;
  
  /* 记得清标志位 */
  frame_flag = 0;
  
  if( (first_byte==(uint8_t)~sec_byte) && (first_byte==IRDA_ID) )
  {
    if( tir_byte == (u8)~fou_byte )
      return tir_byte;
  }
  
  return 0;   /* 错误返回 */
}

// IO 线中断, 接红外接收头的数据管脚
void IRDA_EXTI_IRQHANDLER_FUN(void)
{
	uint8_t pulse_time = 0;
  uint8_t leader_code_flag = 0; /* 引导码标志位，当引导码出现时，表示一帧数据开始 */
  uint8_t irda_data = 0;        /* 数据暂存位 */
  
  if(EXTI_GetITStatus(IRDA_EXTI_LINE) != RESET) /* 确保是否产生了EXTI Line中断 */
	{   
    while(1)
    {
      if( IrDa_DATA_IN()== SET )        /* 只测量高电平的时间 */
      {       
        pulse_time = Get_Pulse_Time();
        
        /* >=5ms 不是有用信号 当出现干扰或者连发码时，也会break跳出while(1)循环 */
        if( pulse_time >= 250 )                
        {
          break; /* 跳出while(1)循环 */
        }
        
        if(pulse_time>=200 && pulse_time<250)         /* 获得前导位 4ms~4.5ms */
        {
          leader_code_flag = 1;
        }
        else if(pulse_time>=10 && pulse_time<50)      /* 0.56ms: 0.2ms~1ms */
        {
          irda_data = 0;
        }
        else if(pulse_time>=50 && pulse_time<100)     /* 1.68ms：1ms~2ms */
        {
          irda_data =1 ; 
        }        
        else if( pulse_time>=100 && pulse_time<=200 ) /* 2.1ms：2ms~4ms */
        {/* 连发码，在第二次中断出现 */
          frame_flag = 1;               /* 一帧数据接收完成 */
          frame_cnt++;                  /* 按键次数加1 */
          isr_cnt ++;                   /* 进中断一次加1 */
          break;                        /* 跳出while(1)循环 */
        }
        
        if( leader_code_flag == 1 )
        {/* 在第一次中断中完成 */
          frame_data <<= 1;
          frame_data += irda_data;
          frame_cnt = 0;
          isr_cnt = 1;
        }
      }      
    } 
		EXTI_ClearITPendingBit(IRDA_EXTI_LINE);     //清除中断标志位

	}  
}

void hwControl(uint8_t key_val){
	
	switch( key_val )
      {
				case 0:
        macLED1_TOGGLE();  
          UsartPrintf(USART_DEBUG,"\r\n key_val=%d \r\n",key_val);
          UsartPrintf(USART_DEBUG,"\r\n Error \r\n");
        break;
        
        case 162:
         macLED1_TOGGLE(); 
          UsartPrintf(USART_DEBUG,"\r\n key_val=%d \r\n",key_val);
          UsartPrintf(USART_DEBUG,"\r\n POWER \r\n");
        break;
        
        case 226:
          macLED1_TOGGLE();
          UsartPrintf(USART_DEBUG,"\r\n key_val=%d \r\n",key_val);
          UsartPrintf(USART_DEBUG,"\r\n MENU \r\n");
        break;
        
        case 34:
           macLED1_TOGGLE(); 
          UsartPrintf(USART_DEBUG,"\r\n key_val=%d \r\n",key_val);
          UsartPrintf(USART_DEBUG,"\r\n TEST \r\n");
        break;
        
        case 2:
          macLED1_TOGGLE();
          UsartPrintf(USART_DEBUG,"\r\n key_val=%d \r\n",key_val);
          UsartPrintf(USART_DEBUG,"\r\n + \r\n");
        break;
        
        case 194:
          macLED1_TOGGLE();
          UsartPrintf(USART_DEBUG,"\r\n key_val=%d \r\n",key_val);
          UsartPrintf(USART_DEBUG,"\r\n RETURN \r\n");
        break;
        
        case 224:
					ydzt = 1;
					status.running = TRUE;
				
					MSD_ENA(DISABLE);
					DelayXms(500);
					
					MotorFz();
				
          UsartPrintf(USART_DEBUG,"\r\n key_val=%d \r\n",key_val);
        break;
        
        case 168:
          macLED1_TOGGLE();
          UsartPrintf(USART_DEBUG,"\r\n key_val=%d \r\n",key_val);
          UsartPrintf(USART_DEBUG,"\r\n > \r\n");
        break;
        
        case 144:
					
					ydzt = 1;
					status.running = TRUE;
				  MSD_ENA(DISABLE);
					DelayXms(500);
				  
          MotorRun();
          UsartPrintf(USART_DEBUG,"\r\n key_val=%d \r\n",key_val);
        break;
        
        case 104:
          macLED1_TOGGLE();
          UsartPrintf(USART_DEBUG,"\r\n key_val=%d \r\n",key_val);
          UsartPrintf(USART_DEBUG,"\r\n 0 \r\n");
        break;
        
        case 152:
          macLED1_TOGGLE();
          UsartPrintf(USART_DEBUG,"\r\n key_val=%d \r\n",key_val);
          UsartPrintf(USART_DEBUG,"\r\n - \r\n");
        break;
        
        case 176:
          macLED1_TOGGLE();
          UsartPrintf(USART_DEBUG,"\r\n key_val=%d \r\n",key_val);
          UsartPrintf(USART_DEBUG,"\r\n C \r\n");
        break;
        
        case 48:
          macLED1_ON();
          UsartPrintf(USART_DEBUG,"\r\n key_val=%d \r\n",key_val);
          UsartPrintf(USART_DEBUG,"\r\n 1 \r\n");
        break;
        
        case 24:
          macLED2_ON();
          UsartPrintf(USART_DEBUG,"\r\n key_val=%d \r\n",key_val);
          UsartPrintf(USART_DEBUG,"\r\n 2 \r\n");
        break;
        
        case 122:
          macLED3_ON();
          UsartPrintf(USART_DEBUG,"\r\n key_val=%d \r\n",key_val);
          UsartPrintf(USART_DEBUG,"\r\n 3 \r\n");
        break;
        
        case 16:
          macLED1_OFF();
          UsartPrintf(USART_DEBUG,"\r\n key_val=%d \r\n",key_val);
          UsartPrintf(USART_DEBUG,"\r\n 4 \r\n");
        break;
        
        case 56:
          macLED2_OFF();
          UsartPrintf(USART_DEBUG,"\r\n key_val=%d \r\n",key_val);
          UsartPrintf(USART_DEBUG,"\r\n 5 \r\n");
        break;
        
        case 90:
          macLED3_OFF();
          UsartPrintf(USART_DEBUG,"\r\n key_val=%d \r\n",key_val);
          UsartPrintf(USART_DEBUG,"\r\n 6 \r\n");
        break;
        
        default:       
        break;
      }      
 }

/*********************************************END OF FILE**********************/
