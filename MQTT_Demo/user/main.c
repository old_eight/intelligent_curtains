/**
	************************************************************
	************************************************************
	************************************************************
	*	文件名： 	main.c
	*
	*	作者： 		麻建
	*
	*	日期： 		2018-2-14
	*
	*	版本： 		V1.0
	*
	*	说明： 		主函数
	*
	*	修改记录：	
	************************************************************
	************************************************************
	************************************************************
**/

//单片机头文件
#include "stm32f10x.h"

//网络协议层
#include "onenet.h"

//网络设备
#include "esp8266.h"

//硬件驱动
#include "delay.h"
#include "led.h"
#include "usart.h"
#include "bsp_dht11.h"
#include "bsp_beep.h"
#include "bsp_irda.h" 
#include "MicroStepDriver.h"  
#include "MSD_test.h"  
#include "bsp_exti.h"

//C库
#include <string.h>

#define CLI()      __set_PRIMASK(1)		/* 关闭总中断 */  
#define SEI() __set_PRIMASK(0)				/* 开放总中断 */ 

extern uint8_t  frame_flag;
extern uint8_t  isr_cnt;
extern uint8_t  frame_cnt;
extern volatile uint8_t ucTcpClosedFlag;
extern int ydzt;



/*
************************************************************
*	函数名称：	Hardware_Init
*
*	函数功能：	硬件初始化
*
*	入口参数：	无
*
*	返回参数：	无
*
*	说明：		初始化单片机功能以及外接设备
************************************************************
*/
void Hardware_Init(void)
{
	NVIC_PriorityGroupConfig(NVIC_PriorityGroup_2);	//中断控制器分组设置
	

	Delay_Init();									    //systick初始化
	
	Usart1_Init(115200);							//串口1，打印信息用
	
	Usart2_Init(115200);							//串口2，驱动ESP8266用
	
	LED_Init();										    //LED初始化
	
	DHT11_Init ();                    //温湿度初始化
	
	Beep_Init ();                     //蜂鸣器初始化
	
	KEY_GPIO_Config();								//按键模拟限位开关初始化
	
	MSD_Init();												//步进电机初始化
	
	UsartPrintf(USART_DEBUG, " Hardware init OK\r\n");
	
}

/*
************************************************************
*	函数名称：	main
*
*	函数功能：	
*
*	入口参数：	无
*
*	返回参数：	0
*
*	说明：		
************************************************************
*/
int main(void)
{
	
	unsigned short timeCount = 0;	//发送间隔变量
	
	unsigned char *dataPtr = NULL;
	
	uint8_t key_val;
	
	uint8_t key_b = 0;
	
	Hardware_Init();				//初始化外围硬件
	
	
	NVIC_SetPriority (SysTick_IRQn, 0);
//	
//	/* 初始化红外接收头CP1838用到的IO */
	IrDa_Init();  
	
	ESP8266_Init();					    //初始化ESP8266
	
	while(OneNet_DevLink())			//接入OneNET
		DelayXms(500);

	
	while(1)
	{
		
		if( frame_flag == 1 ) /* 一帧红外数据接收完成 */
    {
      key_val = IrDa_Process();
      UsartPrintf(USART_DEBUG,"\r\n key_val=%d \r\n",key_val);
      UsartPrintf(USART_DEBUG,"\r\n frame_cnt=%d \r\n",frame_cnt);
      UsartPrintf(USART_DEBUG,"\r\n isr_cnt=%d \r\n",isr_cnt);
      
      hwControl(key_val);
      
		}
		if(++timeCount >= 500)									//发送间隔5s
		{
			UsartPrintf(USART_DEBUG, "OneNet_SendData\r\n");
			OneNet_SendData();									//发送数据
			
			timeCount = 0;
			ESP8266_Clear();
		}
		
		dataPtr = ESP8266_GetIPD(0);
		if(dataPtr != NULL)
			OneNet_RevPro(dataPtr);
		
		key_b = Key_Scan(GPIOC,GPIO_Pin_13);
		if(key_b){
				MSD_ENA(DISABLE);
		}
		
		DelayXms(10);
	
	}

}
